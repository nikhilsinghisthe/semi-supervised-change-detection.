# Semi-Supervised Change Detection.

Multi-class change detection with a limited amount of multi-class change labels.
More about the project: [Here](http://www.classic.grss-ieee.org/earthvision2021/challenge.html)

# Approach
To calculate the multiclass change between two VHR sentinel images:
Take a sentinel cube:
- extract the First 3 bands(RBG) from it.
- divide each 1024 * 1024 image into 16 (256 * 256) non overlapping patches.
Take the second sentinel cube one month older but of the same spot.
- extract the First 3 bands(RBG) from it.
- divide each 1024 * 1024 image into 16 (256 * 256) non overlapping patches.
The two pathes now move into an encoder shown below:
![SNUNet](SNUNet.png)

This architecture wasn't made as an encoder by the original authors in [Siam-NestedUNet] (https://ieeexplore.ieee.org/document/9355573).
Moreover it was used as a binary change detection and in fully supervised condition.

I wanted to try using this model under semi supervised condition. Hence I fused it with the semi supervised semantic segmentation model named [Semi-supervised Semantic Segmentation with Directional Context-aware Consistency (CAC)](https://jiaya.me/papers/semiseg_cvpr21.pdf)

The architecture is shown below:
![CAC](fig.png)

The orginal paper has used DeepLabV3 as the image data encoder which is replaced by the SNUNet_ECAM shown above and the rest of the architecture is same.

## Experiments
- I tried various combination of bands(10 available bands in sentinel 2 imagery)
- Performed an abalation with removing and adding the Channel attention Layer from the SNUNet_ECAM model.
- Tried Cross-Entropy Loss
- Tried weighted Cross-Entropy Loss
- Tried Dice Loss
- Tried Focal Loss
- Tried combination of Dice and Focal Loss

## Postscript
The training was very slow and I missed my deadline to submit to the challange.









